# Listening and Active Communication

## 1. Active Listening

### What are the steps/strategies to do Active Listening?
Active Listening is the act of listening with full attention and with all the senses. It involves the understanding of both the verbal and non-verbal messages of the sender.

The steps to listen actively are as follows:
- The Focus should be on the speaker and topic and the mind should not be distracted
- The speaker should be allowed to finish before responding
- Phrases like "Go ahead", and “Sounds interesting" should be used to keep the speaker interested in the conversation
- Appropriate body language should be used
- Paraphrasing should be used to make sure that both the speaker and the listener are on the same page
- Notes should be taken if needed

## 2. Reflective Listening

### According to Fisher's model, what are the key points of Reflective Listening?

Reflective listening is the act of listening to the speaker with full attention and then communicating back to the speaker to confirm that the message has been understood correctly.

Some key points of Fisher's model are:
- Less talking and more listening
- Using paraphrasing to confirm to the speaker that the understanding is correct
- Understanding the speaker's emotions and not just the facts and ideas
- Responding with empathy, understanding and acceptance
- The response should be related to the topic of communication
- Understanding the message from the speaker's perspective and not having prejudices

## 3. Reflection

### What are the obstacles in your listening process?
The obstacles in my listening process are:
- Random thoughts in mind
- Environmental distractions
- Impatience
- Boredom
- Prejudices

### What can you do to improve your listening?

The steps that I can take to improve my listening are:
- Paying full attention to the speaker while listening
- Avoid random thoughts in mind
- Reducing the environmental distractions
- Showing proper body language to the speaker to non-verbally communicate that I am listening
- Try understanding the message from the speaker's point of view
- Listen patiently

## 4. Types of Communication

### When do you switch to Passive communication style in your day to day life?

Passive communication is a type of communication in which a person sets aside his desires and needs and instead focuses on the needs, desires and opinions of others. 

In my day-to-day life, I switch to passive communication when I tend to please people. Pleasing people involves taking their needs and opinions into consideration while ignoring our own even though the situation becomes quite uncomfortable. Also, when the other person is in a respectable position, I tend to ignore my needs and opinions, thus, involved in passive communication.

### When do you switch into Aggressive communication styles in your day to day life?

Aggressive communication involves stating our opinions, needs and desires aggressively even though it leads to the disrespecting of others.

I switch to an aggressive communication style when I think that my opinions and needs are being ignored by the other person. Also, at times when I am very upset,  I switch to aggressive communication sometimes.

### When do you switch into Passive Aggressive (sarcasm/gossiping/taunts/silent treatment and others) communication styles in your day to day life?

Passive aggressive communication involves expressing aggression indirectly rather than openly showing it.

I switch to this style sometimes when I cannot switch to aggressive communication with the other person. Also, when I do not like a particular act of someone and also cannot communicate my feelings, I switch to this style.

### How can you make your communication assertive? You can analyse the videos and then think what steps you can apply in your own life? (Watch the videos first before answering this question.)

Assertive communication involves expressing our opinions and needs clearly without being aggressive or disrespectful.

The steps that I can apply in my own life to make my communication assertive are:

- Naming my feelings and emotions so they are easier to communicate
- Identify my actual needs and opinions to avoid vagueness in my communication
- I can start using assertive communication in easy situations and with people I am comfortable with. After that, I can apply it to complex situations
- Take care of my body language while communicating so that the communication does not become aggressive
- I can start practising assertive communication as soon as possible as it will be healthier for my relationships
- I can be more cautious about my communication style so that I can avoid using aggressive or passive communication styles.
