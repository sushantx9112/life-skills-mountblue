1. Your takeaways from the video (Minimum 5 points)

In this video, it was explained that the Tiny Habits method can help people build their ideal lives by focusing on small actions that can be done in less than thirty seconds. The three elements that drive behavior are motivation, ability, and prompt. The B=MAP model can be used to make new habits easier. Celebrating after each successful completion of a habit can help wire in the memory of that moment. Consistently improving by just 1% can lead to remarkable improvement.

2. Your takeaways from the video in as much detail as possible

The Tiny Habits method involves focusing on small, specific actions that can be completed in less than thirty seconds. Removing the prompt for an undesired behavior is the best first move to stop it from happening. Motivation, ability, and prompt are the three elements that drive behavior, and understanding these elements can help form new habits. The B=MAP model suggests increasing skills, getting tools and resources, and making the behavior tiny to make forming new habits easier. Celebrating after completing a habit can help wire in the memory of that moment and reinforce the behavior. Consistently improving by just 1% can lead to significant improvements over time.

3. How can you use B = MAP to make making new habits easier?

The B=MAP model can be used to make forming new habits easier by focusing on three simple variables: increasing skills, getting tools and resources, and making the behavior tiny. By researching more on the habit you're focusing on, finding tools and resources to help with the new habit, and making the behavior tiny, it becomes easier to form new habits.

4. Why it is important to "Shine" or Celebrate after each successful completion of habit?

Celebrating after completing a habit can help wire in the memory of that moment and reinforce the behavior. If you don't remember to do a habit, you won't do it. Celebrating during the new habit's performance can also be beneficial.

5. Your takeaways from the video (Minimum 5 points)-1% Better Every Day Video

Improving by just 1% consistently can lead to remarkable improvement over time. The principle of 'aggregate marginal gains' suggests that small gains add up to significant improvements. Consistently improving by just 1% can lead to tremendous improvements.

6. Write about the book's perspective on habit formation from the lens of Identity, processes and outcomes?

Atomic habits are small practices or routines that are the source of incredible power and a component of the system of compound growth. Bad habits repeat themselves because of the wrong system for change. Changes that seem small and unimportant at first can lead to remarkable results over time.

7. Write about the book's perspective on how to make a good habit easier?

To make a good habit easier, you can make it obvious by filling out the Habits Scorecard and becoming aware of your current habits. You can make it attractive by using temptation bundling and pairing an action you want to do with an action you need to do. You can make it easy by reducing friction and decreasing the number of steps between you and your good habits. Finally, you can make it satisfying by finding ways to make the habit rewarding.

8. Write about the book's perspective on making a bad habit more difficult?

Our brains can work against us when we try to overcome bad habits, as these routines can become hardwired in our brains. Making bad habits less convenient by increasing the number of steps to perform them or making the process difficult can help break them. By contrast, making good habits more convenient can make them easier to perform.


