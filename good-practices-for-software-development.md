# Good Practices for Software Development

## Question 1: What is your one major takeaway from each one of the 6 sections. So 6 points in total.

My major takeaways from each section are as follows:
- I need to take feedback frequently so that I understand the requirements well
- In case a deadline will get missed I need to inform relevant team members about the new deadline
- I need to explain the problem clearly and the fixes tried by me
- I need to know my team members well in order to improve our communication
- I can send all my queries in one message rather than sending many messages
- Work ethics should be followed in remote work as well

## Question 2: Which area do you think you need to improve on? What are your ideas to make progress in that area?

I need to follow the motto "Work when you work, play when you play" more strictly. I can progress in this area in the following ways:
- I can have a fixed schedule to do fun activities
- I can minimise the disturbance during work so that my mind does not get distracted
- I can include exercise in my daily routine and follow a healthy diet to remain energetic and focused in work
